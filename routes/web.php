<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoriesController;
use App\Http\Controllers\ProductController;
use App\Models\User;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => LaravelLocalization::setLocale()], function()
{
Route::get('/home', [App\Http\Controllers\HomeController::class,'index'])->name('home');
//admin routes
Route::group(['prefix' => 'admin'], function () {
    Route::get(LaravelLocalization::transRoute('/home'), function () {
    $users[] = Auth::user();
    $users[] = Auth::guard()->user();
    $users[] = Auth::guard('admin')->user();
    return view('admin.home');
    })->name('home');

    
    //user crud routes
    Route::get(LaravelLocalization::transRoute('/users'), [Usercontroller::class ,'index']);
    Route::post(LaravelLocalization::transRoute('/add-user') , [Usercontroller::class, 'adduser']);
    Route::get(LaravelLocalization::transRoute('/edit-user/{id}') ,[Usercontroller::class, 'edit']);
    Route::post(LaravelLocalization::transRoute('/update'),[Usercontroller::class, 'update']); 
    Route::get(LaravelLocalization::transRoute('/delete-user/{id}'),[Usercontroller::class, 'delete']);

    //files routes
    Route::get(LaravelLocalization::transRoute('/file'), [filecontroller::class, 'create']);
    Route::post(LaravelLocalization::transRoute('/file'), [filecontroller::class, 'store']);


    //category routes
    route::get(LaravelLocalization::transRoute('/add-category'),[CategoriesController::class,'addCategory']);
     route::post(LaravelLocalization::transRoute('/add-category'),[CategoriesController::class,'StoreCategory'])->name('category.store');
     route::get(LaravelLocalization::transRoute('/index'),[CategoriesController::class,'categories']);
      route::get(LaravelLocalization::transRoute('/edit-category/{id}'),[CategoriesController::class,'editcategory']);
      route::post(LaravelLocalization::transRoute('/update-category'),[CategoriesController::class,'updatecategory'])->name('category.update');
     route::get(LaravelLocalization::transRoute('/delete-category/{id}'),[CategoriesController::class,'deletecategory']);

//products routes
 route::get(LaravelLocalization::transRoute('/products'),[ProductController::class,'products'])->name('products');
route::get(LaravelLocalization::transRoute('/add-product'),[ProductController::class,'addProduct']);
route::post(LaravelLocalization::transRoute('/add-product'),[ProductController::class,'StoreProduct'])->name('product.store');
route::get(LaravelLocalization::transRoute('/edit-product/{id}'),[ProductController::class,'editproduct']);
route::post(LaravelLocalization::transRoute('/update-product'),[ProductController::class,'updateproduct'])->name('product.update');
route::get(LaravelLocalization::transRoute('/delete-product/{id}'),[ProductController::class,'deleteproduct']);


//authentication routes with the admin
  Route::get(LaravelLocalization::transRoute('/login'), 'App\Http\Controllers\AdminAuth\LoginController@showLoginForm')->name('login');
  Route::post(LaravelLocalization::transRoute('/login'), 'App\Http\Controllers\AdminAuth\LoginController@login');
  Route::post('/logout', 'App\Http\Controllers\AdminAuth\LoginController@logout')->name('logout');
  Route::get('/register', 'App\Http\Controllers\AdminAuth\RegisterController@showRegistrationForm')->name('register');
  Route::post('/register', 'App\Http\Controllers\AdminAuth\RegisterController@register');
  Route::post('/password/email', 'App\Http\Controllers\AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
  Route::post('/password/reset', 'App\Http\Controllers\AdminAuth\ResetPasswordController@reset')->name('password.email');
  Route::get('/password/reset', 'App\Http\Controllers\AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
  Route::get('/password/reset/{token}', 'App\Http\Controllers\AdminAuth\ResetPasswordController@showResetForm');
});
});