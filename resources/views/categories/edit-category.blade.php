<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit category</title>
    <link href="{{ asset('admin/assets/libs/flot/css/float-chart.css') }}"
        rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('admin/dist/css/style.min.css') }}"
        rel="stylesheet">
         <meta name="csrf-token"
        content="{!! csrf_token() !!}">
</head>
<body>
<section style="padding-top:60px;"></section>
<div class="container">
<div class="row">
<div class="col-md-6 offset-md-3">
<div class="card">
<div class="card-header">
Edit category
</div>
<div class="card-body">
@if(Session::has('category_updated'))
<div class="alert alert-success" role="alert">
{{Session::post('category_updated')}}
</div>
@endif
<form method="post" action="{{route('category.update')}}" enctype="multipart/form-data" >
@csrf
<input type="hidden" name="id" value="{{$category->id}}"/>
<div class="form-group">
<label for="name_ar">Name_ar</label>
<input type="text" name="name_ar" value="{{$category->name_ar}}" class="form-control">
</div>
<div class="form-group">
<label for="name_en">Name_en</label>
<input type="text" name="name_en" value="{{$category->name_en}}" class="form-control"/>
</div>
<div class="form-group">
<label for="file">choose Image</label>
<input type="file" name="file" class="form-control" onchange="previewFile(this)"/>
<img id="previewimg" alt="image" src="{{asset('images')}}/{{$category->image}}" style="max-width:130px; margin-top:20px"/>
</div>

<button type="submit" class="btn btn-primary">Submit</button>
<a href="{{ LaravelLocalization::localizeUrl('/admin/index') }}" class="btn btn-danger">back</a>
</form>
</div>
</div>
</div>
</div>
</div>


    <script src="{{asset('admin\assets\libs\flot\jquery.js')}}"></script>
    <script src="{{asset('admin\assets\libs\popper.js\dist\popper.min.js')}}"></script>
    <script src="{{asset('admin\assets\libs\bootstrap\dist\js\bootstrap.min.js')}}"></script>

<script>
function previewFile(input){
    var file=$("input[type=file]").post(0).files[0];
    if (file)
    {
var reader = new FileReader();
reader.onload = function(){
    $('#previewimg').attr("src",reader.result);
}

 reader.readAsDataURL(file); 
    }
   
}
</script>
</body>
</html>