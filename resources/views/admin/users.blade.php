<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0">
    <title>users crud</title>
    <link href="{{asset('admin\assets\libs\bootstrap\dist\css\bootstrap.min.css')}}"
        rel="stylesheet">
    <meta name="csrf-token"
        content="{!! csrf_token() !!}">
</head>


<body>
    <section style="padding-top:60px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">
                            Users <a href="#"
                                class="btn btn-success"
                                data-toggle="modal"
                                data-target="#usersModalCenter">Add New User</a>
                            <a href="{{ LaravelLocalization::localizeUrl('/admin/home') }}"
                                class="btn btn-danger">back</a>
                        </div>
                        <div class="card-body">
                            <table id="usertable"
                                class="table">
                                <thead class="thead-dark">
                                    <tr>
                                        <th>name</th>
                                        <th>email</th>
                                        <!--  <th>password</th>-->
                                        <th>photo</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody class="cont-data text-centre">
                                    @foreach ($users as $user)
                                    <tr id="{{$user->id}}">
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <!--<td>{{$user->password}}</td>-->
                                        {{$user->photo}}
                                        <td>
                                            <button class="btn btn-info edit"
                                                data-route="{{ LaravelLocalization::localizeUrl('/admin/edit-user/'.$user->id) }}"
                                                data-toggle="modal"
                                                data-target="#usersEditModalCenter">Edit <i class="fa fa-edit"></i>
                                            </button>
                                        </td>
                                        <td> <button class="btn btn-danger delete"
                                                data-route="{{ LaravelLocalization::localizeUrl('/admin/delete-user/'.$user->id) }}" >Delete <i
                                                    class="fa fa-times"></i> </button>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Modal Add new user -->
    <div class="modal fade"
        id="usersModalCenter"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered"
            role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"
                        id="exampleModalLongTitle">Add new users</h5>
                    <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul id="error"
                        class="list-unstyled"></ul>
                    <form id="userform">
                        <!-- method="post" action="{{url('file')}}" enctype="multipart/form-data"-->
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text"
                                class="form-control"
                                name="name" />
                        </div>

                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text"
                                class="form-control"
                                name="email" />
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text"
                                class="form-control"
                                name="password" />
                        </div>

                        <!--  <div class="input-group hdtuto control-group lst increment">
                            <input type="file"
                                name="img_name[]"
                                class="myfrm form-control" id="photo">
                            <div class="input-group-btn">
                                <button class="btn btn-success"
                                    type="button"><i class="fldemo glyphicon glyphicon-plus"></i>Add</button>
                            </div>
                        </div>
                        <div class="clone hide">
                            <div class="hdtuto control-group lst input-group"
                                style="margin-top:10px">
                                <input type="file"
                                    name="img_name[]"
                                    class="myfrm form-control">
                                <div class="input-group-btn">
                                    <button class="btn btn-danger"
                                        type="button"><i class="fldemo glyphicon glyphicon-remove"></i> Remove</button>
                                </div>
                            </div>
                        </div>-->

                        <!--<div class="form-group">
                   <label for="photo">photo</label>
                   <input type="text" class="form-control" id="photo"/>
                   </div>-->

                        <div class="modal-footer">
                            <button type="button"
                                class="btn btn-secondary"
                                data-dismiss="modal">Close</button>
                            <input type="submit"
                                class="btn btn-primary"
                                value="Save changes"
                                name="submit">
                        </div>
                       
                    </form>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal Edit user -->
    <div class="modal fade"
        id="usersEditModalCenter"
        tabindex="-1"
        role="dialog"
        aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered"
            role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"
                        id="exampleModalLongTitle">Edit users</h5>
                    <button type="button"
                        class="close"
                        data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul id="error"
                        class="list-unstyled"></ul>
                    <form id="userEditform">
                        @csrf
                        
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text"
                                class="form-control"
                                id="name2"
                                name="name" />
                                <input type="hidden"
                            id="id"
                            name="id" />
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="text"
                                class="form-control"
                                id="email2" 
                                name="email" />
                        </div>
                       <!--<div class="form-group">
                            <label for="password">password</label>
                            <input type="text"
                                class="form-control"
                                id="password2"
                                name="password" />
                        </div>-->

                </div>
                <div class="modal-footer">
                    <button type="button"
                        class="btn btn-secondary"
                        data-dismiss="modal">Close</button>
                    <input type="submit"
                        class="btn btn-primary"
                        value="Save changes"
                        name="submit">
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    <script src="{{asset('admin\assets\libs\flot\jquery.js')}}"></script>
    <script src="{{asset('admin\assets\libs\popper.js\dist\popper.min.js')}}"></script>
    <script src="{{asset('admin\assets\libs\bootstrap\dist\js\bootstrap.min.js')}}"></script>


    <script>
    //multiupload
    $(document).ready(function() {
        $(".btn-success").click(function() {
            var lsthmtl = $(".clone").html();
            $(".increment").after(lsthmtl);
        });

        $("body").on("click", ".btn-danger", function() {
            $(this).parents(".hdtuto").remove();
        });

    });

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //add user
    $("#userform").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(jQuery('#userform')[0]);
        // console.log(formData);
        $.ajax({
            url: "{{LaravelLocalization::localizeURL('/admin/add-user/') }} ",
            type: "POST",
            data: formData,
            contentType: false,
            processData: false,
            success: function(dataBack) {
                // console.log(dataBack);
                $("#error").html(
                    "<li class='alert alert-success text-center p-1'> Added Success </li>");
                $(".cont-data").prepend(dataBack)
                $('#usersModalCenter').modal('hide')

            },
            error: function(xhr, status, error) {

                // console.log(xhr.responseJSON.errors);
                $.each(xhr.responseJSON.errors, function(key, item) {

                    $("#error").html("<li class='alert alert-danger text-center p-1'>" +
                        item + " </li>");
                })
            }
        })

    })

    //delete user
    $(document).on("click", ".delete", function() {

        var route = $(this).attr("data-route");
        $.ajax({
            type: "get",
            url: route,
            success: function(data) {
                alert(data.success);
                $("#" + data.id).remove();

            }


        })
    });

    //Edituser
    $(document).on("click", ".edit", function() {

        var route = $(this).attr("data-route");

        $.ajax({
            type:"get",
            url: route,
            success: function(user) {

                $("#id").val(user.id);
                $("#name2").val(user.name);
                $("#email2").val(user.email);
               // $("#password2").val(user.password);
            }

        })
    });

    //  update  
    $("#userEditform").submit(function(e) {
        e.preventDefault();
        var formData = new FormData(jQuery('#userEditform')[0]);
        var idRow = $("#id").val();

        $.ajax({
            url: "{{LaravelLocalization::localizeURL('/admin/update/')}} ",
            type: "post",
            data: formData,
            contentType: false,
            processData: false,
            success: function(response) {
                $("#errorUpdate").html(
                    "<li class='alert alert-success text-center p-1'> Edited Success </li>");
                $("#" + idRow).html('')
                $("#" + idRow).html(response)
                $('#usersEditModalCenter').modal('hide')
                $("#userEditform")[0].reset();
            }
        })

    });
    </script>
</body>

</html>