<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0">
    <title>Add Product</title>
    <link href="{{ asset('admin/assets/libs/flot/css/float-chart.css') }}"
        rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('admin/dist/css/style.min.css') }}"
        rel="stylesheet">
    <meta name="csrf-token"
        content="{!! csrf_token() !!}">
</head>

<body>
    <section style="padding-top:60px;"></section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="card">
                    <div class="card-header">
                        Add new product
                    </div>
                    <div class="card-body">
                        @if(Session::has('product_added'))
                        <div class="alert alert-success"
                            role="alert">
                            {{Session::get('product_added')}}
                        </div>
                        @endif
                        <form method="post"
                            action="{{route('product.store')}}"
                            enctype="multipart/form-data">
                            @csrf

                            <!-- selection dropdownlist for categories-->
                            <div class="form-group">
                                <label >categories</label>
                                <select name="cat_id"
                                    class="form-control">
                                    @foreach ($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name_ar}}</option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="form-group">
                                <label for="name_ar">Name-Ar</label>
                                <input type="text"
                                    name="name_ar"
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="name_en">Name-En</label>
                                <input type="text"
                                    name="name_en"
                                    class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="price">Price</label>
                                <input type="text"
                                    name="price"
                                    class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="file">choose Image</label>
                                <input type="file"
                                    name="file"
                                    class="form-control"
                                    onchange="previewFile(this)" />
                                <img id="previewimg"
                                    alt="image"
                                    style="max-width:130px; margin-top:20px" />
                            </div>


                            <button type="submit"
                                class="btn btn-primary">Submit</button>
                            <a href="{{ LaravelLocalization::localizeUrl('/admin/products') }}"
                                class="btn btn-danger">back</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="{{asset('admin\assets\libs\flot\jquery.js')}}"></script>
    <script src="{{asset('admin\assets\libs\popper.js\dist\popper.min.js')}}"></script>
    <script src="{{asset('admin\assets\libs\bootstrap\dist\js\bootstrap.min.js')}}"></script>

    <script>
    function previewFile(input) {
        var file = $("input[type=file]").get(0).files[0];
        if (file) {
            var reader = new FileReader();
            reader.onload = function() {
                $('#previewimg').attr("src", reader.result);
            }

            reader.readAsDataURL(file);
        }

    }
    </script>
</body>

</html>