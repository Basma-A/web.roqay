<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0">
    <title>All Products</title>
    <link href="{{ asset('admin/assets/libs/flot/css/float-chart.css') }}"
        rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{ asset('admin/dist/css/style.min.css') }}"
        rel="stylesheet">
    <meta name="csrf-token"
        content="{!! csrf_token() !!}">
</head>

<body>
    <section style="padding-top:60px;"></section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        All Products
                        <a href="{{ LaravelLocalization::localizeUrl('/admin/add-product') }}"
                            class="btn btn-success">Add new Product</a>
                        <a href="{{ LaravelLocalization::localizeUrl('/admin/home') }}"
                            class="btn btn-danger">Back To dashboard</a>
                    </div>
                    <div class="card-body">
                        @if(Session::has('product_deleted'))
                        <div class="alert alert-success"
                            role="alert">
                            {{Session::get('product_deleted')}}
                        </div>
                        @endif
                        <table class="table table-success table-striped">
                            <thead>
                                <tr>
                                    <th>Name_Ar</th>
                                    <th>Name_En</th>
                                    <th>Images</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                @foreach ($products as $product)
                                <tr>
                                    <td>{{$product->name_ar}}</td>
                                    <td>{{$product->name_en}}</td>
                                    <td><img src="{{asset('pimages')}}/{{$product->image}}"
                                            style="max-width:60px;" /></td>
                                    <td>{{$product->price}}</td>
                                    <td>
                                        <a href="{{ LaravelLocalization::localizeUrl('/admin/edit-product/'.$product->id) }}"
                                            class="btn btn-info">Edit</a>

                                        <a href="{{ LaravelLocalization::localizeUrl('/admin/delete-product/'.$product ->id) }}"
                                            class="btn btn-danger">Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="{{asset('admin\assets\libs\flot\jquery.js')}}"></script>
    <script src="{{asset('admin\assets\libs\popper.js\dist\popper.min.js')}}"></script>
    <script src="{{asset('admin\assets\libs\bootstrap\dist\js\bootstrap.min.js')}}"></script>

</body>

</html>