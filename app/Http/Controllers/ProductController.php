<?php

namespace App\Http\Controllers;
use App\Models\Product;
use App\Models\category;
use Illuminate\Http\Request;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Support\Facades\Session;

class ProductController extends Controller
{
    
    public function products(Request $request){
       $products = product::all();
        return view('products.products',compact('products')); 
    }

public function addProduct(){
$categories = category::all();
return view('products.add-product' , compact('categories'));          
}
    
public function StoreProduct(Request $request){
$name_ar = $request->name_ar;
$name_en = $request->name_en;
$price = $request->price;
$image = $request->file('file');
$imageName = time().'.'.$image->getClientOriginalExtension();
$path =public_path('pimages');
$image->move($path,$imageName);

$product = new Product();
$product->name_ar =$name_ar;
$product->name_en = $name_en;
$product->price = $price;
$product->image = $imageName;
$product->cat_id= $request->cat_id;

$product->save();
Return back()->with('product_added','product record has beeen inserted');

    }

     //edit category
    public function editproduct(Product $product , $id){   
    $product = Product::find($id);
    $categories = category::all();
    return view('products.edit-product',compact('categories', 'product'));
    //print_r($category);
}
public function updateproduct(Request $request ,Product $product ){
$name_ar = $request->name_ar;
$name_en = $request->name_en;
$price = $request->price;
$image = $request->file('file');
$imageName = time().'.'.$image->getClientOriginalExtension();
$path =public_path('pimages');
$image->move($path,$imageName);

$product = Product::find($request->id);
$product->name_ar =$name_ar;
$product->name_en = $name_en;
$product->image = $imageName;
$product->cat_id= $request->cat_id; 
$product->save();
Return back()->with('product_updated','product record has beeen updated successfully');

}
//delete category
public function deleteproduct($id){
$product = product::find($id);
unlink(public_path('pimages').'/'.$product->image);
$product->delete();
return back()->with('product_deleted','product record has beeen deleted successfully');

}

}
