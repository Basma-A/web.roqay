<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;

class Usercontroller extends Controller
{
public function index()
{
    $users = User::orderBy('id','DESC')->get();
    
    return view('admin.users',compact('users'));
}

public function adduser (Request $request){
     if($request->ajax())
        {
            $request->validate([
                'name' =>'required|string|max:50',
                'email' =>'required|string|max:50',
                'password' =>'required|string|max:50',
                
            ]);

$user = new User();
$user->name = $request->name;
$user->email = $request->email;
$user->password = $request->password;
$user->save();
$respond['row']  = $user;
return view('admin.users')->with($respond);;
  }
}
public function edit($id){
$user = user::find($id);
return response()->json($user);
}


    public function delete($id)
    {
        user::find($id)->delete();
        return response()->json(['success'=>'Deleted Success','id'=>$id]);
    }


    public function update(Request $request)
    {
        if($request->ajax())
        {
            $request->validate([
                'name' =>'required|string|max:50',
                'email' =>'required|string|max:50',
                 //'password' =>'required|string|max:50',
                
            ]);
            $user = User::findOrFail($request->id);
            $user->name = $request->name;
            $user->email = $request->email;
            //$user->password = $request->password;
            $user->save();
           $respond['row']  = $user;
           return view('admin.rowEdit')->with($respond);
        }
    }
}