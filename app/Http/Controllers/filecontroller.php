<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\File;

class filecontroller extends Controller
{
    public function create()
    {
        return view('create');
    }

     public function store(Request $request)
    {
        $this->validate($request, [
                'filenames' => 'required',
                'filenames.*' => 'image'
        ]);
  
        $files = [];
        if($request->hasfile('img_name'))
         {
            foreach($request->file('img_name') as $file)
            {
                $name = time().rand(1,100).'.'.$file->extension();
                $file->move(public_path('files'), $name);  
                $files[] = $name;  
            }
         }
  
         $file= new File();
         $file->img_name = $files;
         $file->save();
  
        return back()->with('success', 'Data Your files has been successfully added');
    }
}
