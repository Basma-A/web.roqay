<?php

namespace App\Http\Controllers;
use App\Models\category;
use App\Models\Product;
use Illuminate\Contracts\Cache\Store;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Session;


class CategoriesController extends Controller
{
  public function categories(){
       $categories = category::all();
        return view('categories.index',compact('categories')); 
    }
  
    //add category
  public function addCategory(){
return view('categories.add-category');     
    }

 public function StoreCategory(Request $request){
$name_ar = $request->name_ar;
$name_en = $request->name_en;
$image = $request->file('file');
$imageName = time().'.'.$image->getClientOriginalExtension();
$path =public_path('images');
$image->move($path,$imageName);


$category = new category();
$category->name_ar =$name_ar;
$category->name_en = $name_en;
$category->image = $imageName;
$category->save();
Return back()->with('category_added','category record has beeen inserted');

    }

    //edit category
    public function editcategory($id){

     $category = category::find($id);
    return view('categories.edit-category',compact('category'));
    //print_r($category);
}
public function updatecategory(Request $request){
$name_ar = $request->name_ar;
$name_en = $request->name_en;
$image = $request->file('file');
$imageName = time().'.'.$image->getClientOriginalExtension();
$path =public_path('images');
$image->move($path,$imageName);

$category = category::find($request->id);
$category->name_ar =$name_ar;
$category->name_en = $name_en;
$category->image = $imageName; 
$category->save();
Return back()->with('category_updated','category record has beeen updated successfully');

}

//delete category
public function deletecategory($id){
$category = category::find($id);
unlink(public_path('images').'/'.$category->image);
$category->delete();
return back()->with('category_deleted','category record has beeen deleted successfully');

}


}