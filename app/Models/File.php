<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;
    protected $fillable = [
        'img_name'
    ];
    public function setFilenameAttribute($value)
    {
        $this->attributes['img_name'] = json_encode($value);
    }
}
